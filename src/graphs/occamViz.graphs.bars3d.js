/*global $, d3, Highcharts*/

function initOccamVizGraphsBars3d(context) {
  'use strict';

  var Graphs = context.OccamViz.Graphs;

  // Add to our repository of graphing options.
  Graphs._addGraph({
    name:        "3D Bar Chart",
    call:        "bars3d",
    description: "A 3D bar chart."
  });

  /**
   * Draws a bar graph in the specified div selector.
   * @param {String} options.selector The css selector pointing to the div to
   * append the graph.
   * @param {number} options.margin.left The left margin in pixels for the graph.
   * @param {number} options.margin.right The right margin in pixels for the graph.
   * @param {number} options.margin.top The top margin in pixels for the graph.
   * @param {number} options.margin.bottom The bottom margin in pixels for the graph.
   * @param {number} options.width The width in pixels of the graph.
   * @param {String} options.labels.x The text for the x axis (an array when there are multiple datapoints per x).
   * @param {String} options.labelx.y The text for the y axis.
   * @param {String} options.colors The color (described as CSS) to use for
   * the bars. This is an array when there are multiple datapoints per x.
   * @param {Array} options.data.groups The array of datapoints.
   */

  Graphs.prototype.bars3d = function(options) {
    options = options || {};

    options.data     = options.data     || {};
    options.width    = options.width    || 800;
    options.height   = options.height   || 400;
    options.colors   = options.colors   || [];
    options.margin   = options.margin   || {};
    options.labels   = options.labels   || {};
    options.domains  = options.domains  || {};

    // Default labels
    options.labels.x = options.labels.x || "foo";
    options.labels.y = options.labels.y || "bar";

    // Sanitize data
    if (options.data instanceof Array) {
      var array = options.data;
      options.data = {};
      options.data.groups = [{"series": array}];
    }
    else {
      options.data.groups = options.data.groups || [];
    }

    var minGroups = 0;
    var maxGroups = options.data.groups.length;

    if (options.colors.length < maxGroups) {
      for (var i = options.colors.length; i < maxGroups; i++) {
        options.colors.push("hsl(" + ((360 / maxGroups) * i) + ", 60%, 60%)");
      }
    }

    // Set up the chart
    var chart = new Highcharts.Chart({
      chart: {
        renderTo: $(options.selector).attr("id"),
        type: 'column',
        margin: 75,
        options3d: {
          enabled: true,
        alpha: 15,
        beta: 15,
        depth: 50,
        viewDistance: 25
        }
      },
      title: {
        text: 'Chart rotation demo'
      },
      subtitle: {
        text: 'Test options by dragging the sliders below'
      },
      plotOptions: {
        column: {
          depth: 25
        }
      },
      colors: options.colors,
      series: options.data.groups.map(function (e){

        var dict = {};
        dict.name = e.name;
        dict.data = e.series;
        return dict;

      })
    });

    // Activate the sliders
    $('#R0').on('change', function(){
      chart.options.chart.options3d.alpha = this.value;
      showValues();
      chart.redraw(false);
    });
    $('#R1').on('change', function(){
      chart.options.chart.options3d.beta = this.value;
      showValues();
      chart.redraw(false);
    });

    function showValues() {
      $('#R0-value').html(chart.options.chart.options3d.alpha);
      $('#R1-value').html(chart.options.chart.options3d.beta);
    }

    showValues();
  };
}
