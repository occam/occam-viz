/*global $, d3, Highcharts*/

function initOccamVizGraphsnvLineAndBar(context) {
  'use strict';

  var Graphs = context.OccamViz.Graphs;

  // Add to our repository of graphing options.
  /*
  Graphs._addGraph({
    name:        "nv Line and Bar Chart",
    call:        "nvLineAndBar",
    description: "An nv Line and Bar chart."
  });
  */

  /**
   * Draws a bar graph in the specified div selector.
   * @param {String} options.selector The css selector pointing to the div to
   * append the graph.
   * @param {number} options.margin.left The left margin in pixels for the graph.
   * @param {number} options.margin.right The right margin in pixels for the graph.
   * @param {number} options.margin.top The top margin in pixels for the graph.
   * @param {number} options.margin.bottom The bottom margin in pixels for the graph.
   * @param {number} options.width The width in pixels of the graph.
   * @param {String} options.labels.x The text for the x axis (an array when there are multiple datapoints per x).
   * @param {String} options.labelx.y The text for the y axis.
   * @param {String} options.colors The color (described as CSS) to use for
   * the bars. This is an array when there are multiple datapoints per x.
   * @param {Array} options.data.groups The array of datapoints.
   */

  Graphs.prototype.nvLineAndBar = function(options) {
    options = options || {};

    options.data     = options.data     || {};
    options.width    = options.width    || 800;
    options.height   = options.height   || 400;
    options.colors   = options.colors   || [];
    options.margin   = options.margin   || {};
    options.labels   = options.labels   || {};
    options.domains  = options.domains  || {};

    // Default labels
    options.labels.x = options.labels.x || "foo";
    options.labels.y = options.labels.y || "bar";

    // Sanitize data
    if (options.data instanceof Array) {
      var array = options.data;
      options.data = {};
      options.data.groups = [{"series": array}];
    }
    else {
      options.data.groups = options.data.groups || [];
    }

    var minGroups = 0;
    var maxGroups = options.data.groups.length;

    if (options.colors.length < maxGroups) {
      for (var i = options.colors.length; i < maxGroups; i++) {
        options.colors.push("hsl(" + ((360 / maxGroups) * i) + ", 60%, 60%)");
      }
    }

   var chart;
   var color_index = 0;
   var xCord = 0;
	var n = 0;
	var color_index = 0;
	var mydata = options.data.groups.map(function (e){
		
		var dict = {};
		dict.key = e.name;
		if(n == 0)
		dict.bar = true;
		n++;
		dict.values = e.series.map(function (f){
			var d = [];
			d.push(f.x);
			d.push(f.y);
			return d;
		
		});
		dict.color = options.colors[color_index];
		color_index++;
		return dict;
	
	
	});
	
 

nv.addGraph(function() {
    var chart = nv.models.linePlusBarChart()
      .margin({top: 30, right: 60, bottom: 50, left: 70})
      .x(function(d,i) { return i })
      .y(function(d) { return d[1] })
      .color(d3.scale.category10().range())
      ;

    chart.xAxis
      .showMaxMin(false)
	  .axisLabel(options.labels.x);
    	
	
   chart.y1Axis
      .tickFormat(d3.format(',f'))
	  .axisLabel(options.labels.y);	
	
    chart.y2Axis
      .tickFormat(d3.format(',f'))
	  .axisLabel(options.labels.y);

    //chart.bars.forceY([0]);

     d3.select(options.selector)
        .append('svg:svg')
        .attr("class", "chart")
        .attr('preserveAspectRatio', 'xMidYMid')
        .attr('viewBox', '0 0 ' + options.width + ' ' + options.height)
        .attr('width',  options.width  + 'px')
        .attr('height', options.height + 'px')
		.style({
			width: options.width + 'px',
			height: options.height + 'px'
		})
      .datum(mydata)
      .transition().duration(500)
      .call(chart)
      ;

    nv.utils.windowResize(chart.update);

    return chart;
});



  };
}
