/*jslint browser: true*/
/*global $*/

// Compiler directive for UglifyJS.  See occamViz.const.js for more info.
if (typeof DEBUG === 'undefined') {
  DEBUG = true;
}

// LIBRARY-GLOBAL CONSTANTS
//
// These constants are exposed to all library modules.

// GLOBAL is a reference to the global Object.
var Fn = Function, GLOBAL = new Fn('return this')();

// LIBRARY-GLOBAL METHODS
//
// The methods here are exposed to all library modules.  Because all of the
// source files are wrapped within a closure at build time, they are not
// exposed globally in the distributable binaries.

/**
 * A no-op function.  Useful for passing around as a default callback.
 */
function noop () { }

/**
 * Init wrapper for the core module.
 * @param {Object} The Object that the library gets attached to in
 * occamViz.init.js. If the library was not loaded with an AMD loader such as
 * require.js, this is the global Object.
 */
function initOccamVizCore (context) {
  'use strict';

  // PRIVATE MODULE CONSTANTS
  //

  // An example of a CONSTANT variable;
  var CORE_CONSTANT = true;

  // PRIVATE MODULE METHODS
  //
  // These do not get attached to a prototype. They are private utility
  // functions.

  /**
   * This is the constructor for the OccamViz Object.
   * Note that the constructor is also being
   * attached to the context that the library was loaded in.
   * @param {Object} opt_config Contains any properties that should be used to
   * configure this instance of the library.
   * @constructor
   */
  var OccamViz = context.OccamViz = function (opt_config) {
    opt_config = opt_config || {};

    opt_config.domain = opt_config.domain || "";

    // Read only (led by underscores)
    this._domain = opt_config.domain;

    // Accessible
    this.graphs = new OccamViz.Graphs();

    return this;
  };

  // LIBRARY PROTOTYPE METHODS
  //
  // These methods define the public API.

  /**
   * Returns the set domain. An empty string means it is polling the source
   * domain for the script.
   * @return {string}
   */
  OccamViz.prototype.getDomain = function() {
    return this._domain;
  };

  /**
   * Callback for rendering results.
   *
   * @callback resultsCallback
   * @param {Object} results An object defining the results.
   */

  /**
   * Returns the results for the given experiment.
   * @param {number} experiment_id The id of the experiment to query.
   * @param {resultsCallback} success A callback that fires when the results
   * are available.
   * @return {OccamViz}
   */
  OccamViz.prototype.getResults = function(experiment_id, success) {
    var url = this._domain + "/experiments/" + experiment_id + "/results";
    $.getJSON(url, function(data) {
      success(data);
    });

    return this;
  };

  /**
   * This method will sanitize a graph options hash. Typically used internally
   * to fill in the hash with default values, but can be used externally to see
   * default values or use with detached (unregistered) graphing methods.
   */
  OccamViz.sanitizeOptions = function(options) {
    options = options || {};

    options.data     = options.data     || {};
    options.width    = options.width    || 800;
    options.height   = options.height   || 400;
    options.colors   = options.colors   || [];
    options.margin   = options.margin   || {};
    options.labels   = options.labels   || {};
    options.domains  = options.domains  || {};

    // Default labels
    options.labels.x = options.labels.x || {};
    if (typeof options.labels.x == 'string' || options.labels.x instanceof String) {
      options.labels.x = {
        name: options.labels.x
      };
    }
    options.labels.x.series = options.labels.x.series || [];
    options.labels.y = options.labels.y || "bar";

    // Sanitize data
    if (options.data instanceof Array) {
      var array = options.data;
      options.data = {};
      options.data.groups = [{"series": array}];
    }
    else {
      options.data.groups = options.data.groups || [];
    }

    // Set up domains
    var maxPoint = d3.max(options.data.groups, function( d ) {
      return d3.max(d.series);
    });

    var minPoint = d3.min(options.data.groups, function( d ) {
      return d3.min(d.series);
    });

    var minX = 0;
    var maxX = d3.max([options.labels.x.length - 1,
                       d3.max(options.data.groups, function(d) {
                          return d.series.length - 1;
                       })
                      ]);

    var minGroups = 0;
    var maxGroups = options.data.groups.length;

    options.domains.x = options.domains.x || [minX, maxX];
    options.domains.y = options.domains.y || [d3.min([0, minPoint]), maxPoint];

    if (options.colors.length < maxGroups) {
      for (var i = options.colors.length; i < maxGroups; i++) {
        options.colors.push("hsl(" + ((360 / maxGroups) * i) + ", 60%, 60%)");
      }
    }

    options.margin.top    = options.margin.top    || 20;
    options.margin.left   = options.margin.left   || 20;
    options.margin.right  = options.margin.right  || 20;
    options.margin.bottom = options.margin.bottom || 20;

    return options;
  };

  // DEBUG CODE
  //
  // With compiler directives, you can wrap code in a conditional check to
  // ensure that it does not get included in the compiled binaries.  This is
  // useful for exposing certain properties and methods that are needed during
  // development and testing, but should be private in the compiled binaries.
  if (DEBUG) {
  }
}
