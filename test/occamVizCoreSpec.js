describe("occamViz.core", function() {
  describe("constructor", function() {
    it("should default the domain to be empty", function() {
      var occam = new OccamViz();
      expect(occam.getDomain()).toBe("");
    });

    it("should respect custom domain", function() {
      var occam = new OccamViz({domain: "foobar"});
      expect(occam.getDomain()).toBe("foobar");
    });
  });

  describe("getResults", function() {
    it("should call the given callback", function(done) {
      $.mockjax({
        url: '/experiments/4/results',
        responseTime: 100,
        responseText: {
          foo: "bar"
        }
      });

      var occam = new OccamViz();

      occam.getResults(4, function(data) {
        returned = true;
        expect(data.foo).toBe("bar");
        done();
      });
    });

    it("should return this reference", function() {
      $.mockjax({
        url: '/experiments/4/results',
        responseTime: 0,
        responseText: {
          foo: "bar"
        }
      });

      var occam = new OccamViz();

      expect(occam.getResults(4, function(data) {})).toBe(occam);
    });
  });
});
